import csv
import json
import re

import requests
import tushare as ts


class StockForecast(object):
    def __init__(self):
        self.base_url = "http://f10.eastmoney.com/ProfitForecast/ProfitForecastAjax?code={}"
        self.headers = {
            "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36"
        }

    @staticmethod
    def get_stock_info():
        """
        获取当前股票列表
        :return list
        """
        pro = ts.pro_api()
        data = pro.stock_basic(exchange='', list_status='L', fields='ts_code,name')
        stock_info = data.values.tolist()
        return stock_info

    def save_csv(self, item, path, keyword_list, n):
        """保存csv文件"""
        if item:
            with open(path, 'w', newline='', encoding='utf-8') as f:
                d1 = item[0].keys()
                writer = csv.DictWriter(f, delimiter=',', fieldnames=d1)
                header = dict(zip(writer.fieldnames, keyword_list))
                writer.writerow(header)
                writer.writerows(item)
            with open("stock_log.log", "a", encoding="utf-8") as f:
                f.write("抓取完成:%s,%s" % (n, path) + "\n")
        else:
            with open("stock_log.log", "a", encoding="utf-8") as f:
                f.write("抓取失败:%s, %s-----------------------------" % (n, path) + "\n")
            with open("无数据列表.txt", "a", encoding="utf-8") as f:
                f.write(path[11:-4] + "\n")
                
    def run(self):
        stock_info = self.get_stock_info()
        for i in range(len(stock_info)):
            stock_code = stock_info[i][0]
            stock_name = re.sub(r"\*", "", stock_info[i][1])
            code = stock_info[i][0][-2:] + stock_info[i][0][:6]
            # 拼接目标url
            url = self.base_url.format(code)
            response = requests.get(url, headers=self.headers)
            data = response.content.decode()
            dic_data = json.loads(data)

            # 提取预测明细
            ycmx = dic_data.get("ycmx")

            # 抽取净利润、每股收益数据数据
            item_mgsy = ycmx["mgsy"]["data"]
            item_jlr = ycmx["jlr"]["data"]

            # 存储路径
            path_mgsy = "stock_data/%s_%s_%s.csv" % (stock_code, stock_name, "每股收益")
            path_jlr = "stock_data/%s_%s_%s.csv" % (stock_code, stock_name, "净利润")

            # 修改表头
            keyword_list = ["时间", "机构", "研究员", "2018", "2019", "2020预测", "2021预测", "2022预测", "2023预测", "评级"]

            # 保存数据
            print(i, "开始抓取：", "%s_%s_%s.csv" % (stock_code, stock_name, "每股收益"))
            self.save_csv(item_mgsy, path_mgsy, keyword_list, i)

            print(i, "开始抓取：", "%s_%s_%s.csv" % (stock_code, stock_name, "净利润"))
            self.save_csv(item_jlr, path_jlr, keyword_list, i)


if __name__ == '__main__':
    StockForecast().run()
